# 2. Le guide du bon développeur
<!-- .slide: data-state="no-toc-progress" class="no-toc-progress" -->

----

## Dans quel cas ?
<!-- .slide: data-state="no-toc-progress" class="no-toc-progress" -->

* Je développe un serveur
* Je développe un client

----

## Je développe un serveur
<!-- .slide: data-state="no-toc-progress" class="no-toc-progress" -->

* Objectif 100% HTTPS
* Partage de responsabilité

----

## Reponsabilité applicative : checklist
<!-- .slide: data-state="no-toc-progress" class="no-toc-progress" -->

* Forcer l'application à utiliser SSL
<!-- .element: class="fragment" -->

----

## Reponsabilité applicative : forcer l'application à utiliser SSL
<!-- .slide: data-state="no-toc-progress" class="no-toc-progress" -->
web.xml *de l'application* (webapp/WEB-INF/web.xml)
```XML
<security-constraint>
      <web-resource-collection>
          <web-resource-name>Zone HTTPS</web-resource-name>
          <url-pattern>/*</url-pattern>
      </web-resource-collection>
      <user-data-constraint>
          <transport-guarantee>CONFIDENTIAL</transport-guarantee>
      </user-data-constraint>
  </security-constraint>
```

----

## Responsabilité infrastructure : checklist
<!-- .slide: data-state="no-toc-progress" class="no-toc-progress" -->
* Ouvrir le port HTTPS
* Utiliser un "bon" certificat  

On dit "merci le CEI" :)

----

## Responsabilité infrastructure : paramétrage local
<!-- .slide: data-state="no-toc-progress" class="no-toc-progress" -->
Le plan :  
1. Générer un certificat auto-signé
2. Ouvrir le port 8443 de tomcat
3. Dire à tomcat d'utiliser notre certificat

[https://gforge.insee.fr/docman/view.php/626/18108/protection-ressources.pdf](https://gforge.insee.fr/docman/view.php/626/18108/protection-ressources.pdf)

----

## 1. Générer un certificat auto-signé (via Java)
Localiser keytool (C:\Program Files (x86)\insee\atelier-dev-2\applications\jdk18_64\jdk-1.8.0_40\bin\keytool.exe)  
```
 ./keytool.exe -genkey -keyalg RSA -keystore D:\\.keystore
```
Répondre aux questions
```
Quels sont vos nom et prénom ?
  [Unknown]:  localhost
Quel est le nom de votre unité organisationnelle ?
  [Unknown]:
Quel est le nom de votre entreprise ?
  [Unknown]:
Quel est le nom de votre ville de résidence ?
  [Unknown]:
Quel est le nom de votre état ou province ?
  [Unknown]:
Quel est le code pays à deux lettres pour cette unité ?
  [Unknown]:

```
<!-- .element: class="fragment" -->

----

## 2. Ouvrir le port 8443 de tomcat
Localiser server.xml (workspace/Servers/nomDuServeur/server.xml)  
```xml
<!--
  <Connector port="8443" protocol="org.apache.coyote.http11.Http11NioProtocol"
             maxThreads="150" SSLEnabled="true" scheme="https" secure="true"
             clientAuth="false" sslProtocol="TLS" />
  -->
```
Activer le connector 8443

----

## 3. Dire à tomcat d'utiliser notre certificat
(workspace/Servers/nomDuServeur/server.xml)
```xml
<Connector port="8443" protocol="org.apache.coyote.http11.Http11NioProtocol"
             maxThreads="150" SSLEnabled="true" scheme="https" secure="true"
             clientAuth="false" sslProtocol="TLS"
             keystoreFile="D:\.keystore" keyAlias="myKey" keystorePass="changeit"  />
```

----

## Vérification du résultat

* L'application est accessible en HTTPS
* L'accès en HTTP redirige vers le HTTPS
* Le certificat est auto-signé

----

## Je développe un client
<!-- .slide: data-state="no-toc-progress" class="no-toc-progress" -->

* Je veux consommer des données d'un service
* Quel certificat ce service présente t'il ?
<!-- .element: class="fragment" -->
* Quels certificats mon programme reconnait t'il ?
<!-- .element: class="fragment" -->

----

## Quel certificat ce service présente t'il ?
<!-- .slide: data-state="no-toc-progress" class="no-toc-progress" -->

* Allons vérifier !
* Pilot QF : [https://qfpilotlht01.ad.insee.intra/](https://qfpilotlht01.ad.insee.intra/)
* Pilot prod : [https://pilot.insee.fr/](https://pilot.insee.fr/)  
* Spoc QF : [https://qfspoclht01.ad.insee.intra/spoc/](https://qfspoclht01.ad.insee.intra/spoc/)
* Spoc prod : [https://services.insee.fr/mail/](https://services.insee.fr/mail/)  
* INSEE.fr : [https://insee.fr](https://insee.fr)
* API twitter : [https://api.twitter.com/1.1/search/tweets.json](https://api.twitter.com/1.1/search/tweets.json)

----

## Bilan des cas
<!-- .slide: data-state="no-toc-progress" class="no-toc-progress" -->

| Service      | Environnement | Autorité         |
| -------------- | ---------------|------------------------------------- |
| Appli | Local | Auto-signé |
| Appli interne (ex : Pilot) | DEV / QF |  Auto-signé            |
| Appli interne (ex : Pilot) | Prod | AC Subordonnée    |
| Service transverse (ex : SPOC) | DEV / QF        |  AC Subordonnée            |
| Service transverse (ex : SPOC) |  Prod        |  AC Subordonnée            |
| Appli DMZ (ex : insee.fr) | Prod DMZ | Certigna Services CA |
| Site web (ex : google, API twitter)         | Prod internet   | Autorité reconnue |

----

## Quels certificats mon programme reconnait t'il ?
<!-- .slide: data-state="no-toc-progress" class="no-toc-progress" -->
* Windows  
* En Java :
Fichier cacerts dans C:\Program Files (x86)\insee\atelier-dev-2\applications\jdk18_64\jdk-1.8.0_40\jre\lib\security
```
"../../bin/keytool.exe" -list -keystore cacerts
```
<!-- .element: class="fragment" -->  
* En Javascript

----

## Zoom sur Java
<!-- .slide: data-state="no-toc-progress" class="no-toc-progress" -->
* Certificats dans le fichier cacerts  
* JDK packagé INSEE >= Java 7  
* Atelier de dev & Prod

Conlusion à partir du tableau

----

## Java : cas pratique
<!-- .slide: data-state="no-toc-progress" class="no-toc-progress" -->
* Mon serveur et mon client tournent en local

```
Exception in thread "main" com.mashape.unirest.http.exceptions.UnirestException: javax.net.ssl.SSLHandshakeException: sun.security.validator.ValidatorException: PKIX path building failed: sun.security.provider.certpath.SunCertPathBuilderException: unable to find valid certification path to requested target
	at com.mashape.unirest.http.HttpClientHelper.request(HttpClientHelper.java:143)
	at com.mashape.unirest.request.BaseRequest.asString(BaseRequest.java:56)
	at main.MainUnirest.getHello(MainUnirest.java:34)
	at main.MainUnirest.main(MainUnirest.java:16)
Caused by: javax.net.ssl.SSLHandshakeException: sun.security.validator.ValidatorException: PKIX path building failed: sun.security.provider.certpath.SunCertPathBuilderException: unable to find valid certification path to requested target
	at sun.security.ssl.Alerts.getSSLException(Alerts.java:192)
	at sun.security.ssl.SSLSocketImpl.fatal(SSLSocketImpl.java:1959)
	at sun.security.ssl.Handshaker.fatalSE(Handshaker.java:302)
	at sun.security.ssl.Handshaker.fatalSE(Handshaker.java:296)
	at sun.security.ssl.ClientHandshaker.serverCertificate(ClientHandshaker.java:1514)
	at sun.security.ssl.ClientHandshaker.processMessage(ClientHandshaker.java:216)
	at sun.security.ssl.Handshaker.processLoop(Handshaker.java:1026)
	at sun.security.ssl.Handshaker.process_record(Handshaker.java:961)
	...
Caused by: sun.security.validator.ValidatorException: PKIX path building failed: sun.security.provider.certpath.SunCertPathBuilderException: unable to find valid certification path to requested target
	at sun.security.validator.PKIXValidator.doBuild(PKIXValidator.java:397)
	at sun.security.validator.PKIXValidator.engineValidate(PKIXValidator.java:302)
	at sun.security.validator.Validator.validate(Validator.java:260)
	at sun.security.ssl.X509TrustManagerImpl.validate(X509TrustManagerImpl.java:324)
	at sun.security.ssl.X509TrustManagerImpl.checkTrusted(X509TrustManagerImpl.java:229)
	at sun.security.ssl.X509TrustManagerImpl.checkServerTrusted(X509TrustManagerImpl.java:124)
	at sun.security.ssl.ClientHandshaker.serverCertificate(ClientHandshaker.java:1496)
	... 24 more
Caused by: sun.security.provider.certpath.SunCertPathBuilderException: unable to find valid certification path to requested target
	at sun.security.provider.certpath.SunCertPathBuilder.build(SunCertPathBuilder.java:141)
	at sun.security.provider.certpath.SunCertPathBuilder.engineBuild(SunCertPathBuilder.java:126)
	at java.security.cert.CertPathBuilder.build(CertPathBuilder.java:280)
	at sun.security.validator.PKIXValidator.doBuild(PKIXValidator.java:392)
	... 30 more
```
<!-- .element: class="fragment" -->

----

## Java : brainstorming des solutions

GO !  
* Désactiver la vérification
<!-- .element: class="fragment" -->
* Faire signer localhost par l'INSEE
<!-- .element: class="fragment" -->
* Ajouter le certificat cible au runtime
<!-- .element: class="fragment" -->
* Ajouter le certificat cible à cacerts
<!-- .element: class="fragment" -->
* Arrêter de tester en local (coeur coeur gitlab-ci coeur coeur)
<!-- .element: class="fragment" -->


----

## Java : désactiver la vérification
<!-- .slide: data-state="no-toc-progress" class="no-toc-progress" -->
/!\ Pas de ça en prod /!\
```Java
SSLContext sc = SSLContext.getInstance("TLS");
sc.init(null, new TrustManager[] { new TrustAllX509TrustManager() }, new java.security.SecureRandom());
HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
HttpsURLConnection.setDefaultHostnameVerifier( new HostnameVerifier(){
    public boolean verify(String string,SSLSession ssls) {
        return true;
    }
});
```
<!-- .element: class="fragment" -->
```Java
public class TrustAllX509TrustManager implements X509TrustManager {
    public X509Certificate[] getAcceptedIssuers() {
        return new X509Certificate[0];
    }

    public void checkClientTrusted(java.security.cert.X509Certificate[] certs,
            String authType) {
    }

    public void checkServerTrusted(java.security.cert.X509Certificate[] certs,
            String authType) {
    }

}
```
<!-- .element: class="fragment" -->
/!\ Pas de ça en prod /!\

----

## Java : faire signer localhost par l'INSEE
<!-- .slide: data-state="no-toc-progress" class="no-toc-progress" -->
Conclusion possible des slides précédentes :  
* Les certificats INSEE sont reconnus en local
* On a la main sur le certificat utilisé par tomcat

Donc on fait signer localhost par l'INSEE ?

----

## Java : ajouter le certificat cible au runtime
<!-- .slide: data-state="no-toc-progress" class="no-toc-progress" -->
* Etape 1 : récupérer le certificat cible  
* Etape 2 : le transformer en JKS  
```
keytool -import -file localhost -keystore mycerts
```
* Etape 3 : utiliser le JKS comme trustStore applicatif
```
System.setProperty("javax.net.ssl.trustStore","D:\\mycerts");
System.setProperty("javax.net.ssl.trustStorePassword","changeit");
```
ou
```
-Djavax.net.ssl.trustStore=D:\\mycerts  
-Djavax.net.ssl.trustStorePassword=changeit
```  

Avis ?

----

## Java : ajouter le certificat cible à cacerts
<!-- .slide: data-state="no-toc-progress" class="no-toc-progress" -->
* Etape 1 : récupérer le certificat cible  
* Etape 2 : l'ajouter à cacerts
```
keytool -import -file localhost -keystore cacerts
```  

Avis ?

----

## Arrêter de tester en local (coeur coeur gitlab-ci coeur coeur)
<!-- .slide: data-state="no-toc-progress" class="no-toc-progress" -->
Echelle de troll : 8/10

----

## Cas pratique : conclusion
<!-- .slide: data-state="no-toc-progress" class="no-toc-progress" -->
/!\ Désactiver la vérification : toléré en local uniquement /!\  
Privilégier la solution avec -Djavax.net.ssl.trustStore=D:\\mycerts

----

## Je fais du Javascript
<!-- .slide: data-state="no-toc-progress" class="no-toc-progress" -->

* Mon environnement = mon navigateur (avantages / inconvénients)
* Outils de dev
* Mixed content

----

<!-- .slide: data-state="no-toc-progress" class="no-toc-progress" -->
![](images/ymlg.jpg)
