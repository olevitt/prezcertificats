# 1. Bases de cryptographie
<!-- .slide: data-state="no-toc-progress" class="no-toc-progress" -->

----

## Protéger des messages
<!-- .slide: data-state="no-toc-progress" class="no-toc-progress" -->


* En confidentialité : fonctions de chiffrement
* En intégrité : fonctions de signature

----

## Mécanismes de chiffrement
<!-- .slide: data-state="no-toc-progress" class="no-toc-progress" -->
[https://chiffrer.info/](https://chiffrer.info/)  
* Chiffrement symétrique
* Chiffrement asymétrique

----

## Chiffrement symétrique
<!-- .slide: data-state="no-toc-progress" class="no-toc-progress" -->
La même clé permet de chiffrer et déchiffrer  
![](images/clef_symetrique.jpg)  

* Couteux en nombre de clés
* Très efficace en temps de chiffrement  


----

## Chiffrement asymétrique
<!-- .slide: data-state="no-toc-progress" class="no-toc-progress" -->

Une clé publique permet de chiffrer et une clé privée permet de déchiffrer  
![](images/clef_asymetrique.jpg)  

* Efficace en nombre de clés
* Couteux en temps

----

## Implémentation courante
<!-- .slide: data-state="no-toc-progress" class="no-toc-progress" -->

* Chiffrement symétrique : permet de chiffrer les données
* Chiffrement asymétrique : permet d'échanger les secrets symétriques


----

## Récap
<!-- .slide: data-state="no-toc-progress" class="no-toc-progress" -->

* Chiffrement symétrique
* Chiffrement asymétrique
* Implémentation courante

----

## Confiance dans les clés publiques
<!-- .slide: data-state="no-toc-progress" class="no-toc-progress" -->

Garantir l'intégrité lors de la distribution des clés publiques
* PKI : Public Key Infrastructure
* Signature des clés publiques = CERTIFICAT

----

## Autorité de certification
<!-- .slide: data-state="no-toc-progress" class="no-toc-progress" -->

Autorité de certification
* Signe les clés publiques
* Processus de validation du demandeur

----

## Qui c'est?
<!-- .slide: data-state="no-toc-progress" class="no-toc-progress" -->

* Autorité de "portée internationale" dans les magasins de certificats (linux, windows, firefox ...)
* Autorité interne : Insee, Ministère Finances

----

## En pratique
<!-- .slide: data-state="no-toc-progress" class="no-toc-progress" -->

* X509 : inspection
* Magasin de certificats
* TLS ( s de https ou s, de ftps .. ou s de n'importe quelle flux tcp qu'on souhaite protéger => ex : Stunnel)
* [https://ssllabs.com/ssltest/analyze.html?d=insee.fr&latest](https://ssllabs.com/ssltest/analyze.html?d=insee.fr&latest)
