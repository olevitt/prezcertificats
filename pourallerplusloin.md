# 3. Pour aller plus loin
<!-- .slide: data-state="no-toc-progress" class="no-toc-progress" -->

----

## Signature
* Intégrité du message
* Authentifier l'auteur  

----

## Comment signer ?
<!-- .slide: data-state="no-toc-progress" class="no-toc-progress" -->
* Algorithme asymétrique
* Empreinte numérique (hash)

----

## Hashage
<!-- .slide: data-state="no-toc-progress" class="no-toc-progress" -->
* Ensemble de départ énorme
* Ensemble d'arrivée limité
* Rapidité de calcul
* Contrôle rapide mais non complet

| Input      | Taille | MD5 | SHA-1         |
| -------------- | --------| ---------------|------------------------------------- |
| "salut" | 5 | 6aba532a54c9eb9aa30496fa7f22734d | 3775e40fbea098e6188f598cce2a442eb5adfd2c |
| "salutt" | 6 | e298e49fb23091216b61a43e818eec38 | da5b1925d68a67e1b0a79cba8d01dfdb9f24b8ed |
| grofichier | 6309210624 | e943bde85a880f27e895efcbb40e85e5 | e9e46719019edd7104096726ea384caa742d1eb5 |

----

## Hashage : cas d'utilisation
<!-- .slide: data-state="no-toc-progress" class="no-toc-progress" -->
* Somme de contrôle (IBAN, SIRET) : sûreté de fonctionnement
* Intégrité d'un téléchargement (MD5, SHA-1)
* Un fichier a t'il été modifié ?
* Bitcoin (SHA-1)
* hashCode, HashMap, HashSet
* ...

----

## Hashage : attaques & propriétés
<!-- .slide: data-state="no-toc-progress" class="no-toc-progress" -->
Collision, paradoxe des anniversaires  

* Résistance aux attaques de pré-images (antécédants)
* Résistance aux attaques de seconde préimage
* Résistance aux collisions

----

## Hashage : en Java
<!-- .slide: data-state="no-toc-progress" class="no-toc-progress" -->
* java.security.MessageDigest
```
byte[] digest = MessageDigest.getInstance("MD5").digest("salut".getBytes("UTF-8"));
//byte[] digest = MessageDigest.getInstance("SHA-1").digest("salut".getBytes("UTF-8"));
String hash = DatatypeConverter.printHexBinary(digest);
System.out.println(hash);
```
Ou, en utilisant Apache commons codec
```
String hash = DigestUtils.md5Hex("salut");
String hash = DigestUtils.sha1Hex("salut");
```

----

## Mode de chiffrement
[https://fr.wikipedia.org/wiki/Mode_d%27op%C3%A9ration_(cryptographie)](https://goo.gl/nevVzE)
