<!-- .slide: data-state="no-toc-progress" --> <!-- don't show toc progress bar on this slide -->

# Présentation certificats CNIN
<!-- .element: class="no-toc-progress" --> <!-- slide not in toc progress bar -->
Frédéric Comte   
Olivier Levitt

Online : [http://prez.levitt.fr/](http://prez.levitt.fr/)  
Source : [https://bitbucket.org/olevitt/prezcertificats](https://bitbucket.org/olevitt/prezcertificats)  

Ecrit en [markdown][1] et propulsé par [reveal.js](http://lab.hakim.se/reveal-js/)

[1]: https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet
